#!/usr/bin/env python3

import sqlite3

class Jpdb:
	def __init__(self, path):
		self.db_path = path
		self.conn = None

	def __enter__(self):
		self.connect()
		return self
	def __exit__(self, type, value, traceback):
		self.close()

	_sql = {
		"create": {
			"book":
				"""CREATE TABLE IF NOT EXISTS book (
					id integer PRIMARY KEY,
					title text NOT NULL
				);""",
			"lesson":
				"""CREATE TABLE IF NOT EXISTS lesson (
					id integer PRIMARY KEY,
					id_book integer NOT NULL,
					title text NOT NULL,
					FOREIGN KEY (id_book) REFERENCES book (id)
				);""",
			"word_power":
				"""CREATE TABLE IF NOT EXISTS word_power (
					id integer PRIMARY KEY,
					id_lesson integer NOT NULL,
					title text NOT NULL,
					FOREIGN KEY (id_lesson) REFERENCES lesson (id)
				);"""
		},
		"insert": {
			"book":
				"""INSERT INTO book(title)
					VALUES(?);
				""",
			"lesson":
				"""INSERT INTO lesson(id_book, title)
					VALUES(?,?)
				"""
		},
		"select": {
			"generic":
				"""SELECT * FROM {} """,
			"where":
				"""WHERE {} = ? """,
			"lesson":
				"""SELECT id, title FROM lesson
					WHERE id_book = ?
				"""
		},
		"update": {
			"book":
				"""UPDATE BOOK
					SET title = ?
					WHERE id = ?
				""",
			"lesson":
				"""UPDATE LESSON
					SET title = ?
					WHERE id = ?
				"""
		}
	}

	def connect(self):
		self.conn = sqlite3.connect(self.db_path)
	def close(self):
		if self.conn:
			self.conn.close()

	def init(self):
		""" Populate the database with the tables. """
		try:
			cur = self.conn.cursor()
			for table in Jpdb._sql['create']:
				cur.execute(Jpdb._sql['create'][table])
		except sqlite3.Error as e:
			print(e)

	def book_create(self, title):
		cur = self.conn.cursor()
		cur.execute(Jpdb._sql['insert']['book'], (title,))
		self.conn.commit()

	def book_list(self):
		cur = self.conn.cursor()
		cur.execute(Jpdb._sql['select']['generic'].format('book'))
		books = []
		for row in cur.fetchall():
			books.append(Book(self, row[0], row[1]))
		return books

	def book_get(self, title):
		cur = self.conn.cursor()
		cur.execute(Jpdb._sql['select']['generic'].format('book') + Jpdb._sql['select']['where'].format('title'), (title,))
		book = cur.fetchone()
		if book == None:
			raise LookupError("Could not find the specified book.", title)
		return Book(self, book[0], book[1])

	def info(self):
		cur = self.conn.cursor()
		cur.execute("select sql from sqlite_master where sql not NULL")
		for row in cur.fetchall():
			print(row)


class Book:
	def __init__(self, db, id, title):
		self.db = db
		self.id = id
		self.title = title

	def header():
		return (
			"  id  title\n" +
			"-------------"
		)
	def __str__(self):
		return "{:>4}  {}".format(self.id, self.title)
	def doprint(books):
		print(Book.header())
		if type(books) != list:
			books = [ books ]
		for book in books:
			print(book)

	def update(self):
		cur = self.db.conn.cursor()
		cur.execute(Jpdb._sql['update']['book'], (self.title, self.id))
		self.db.conn.commit()

	def lesson_create(self, title):
		cur = self.db.conn.cursor()
		cur.execute(Jpdb._sql['insert']['lesson'], (self.id, title))
		self.db.conn.commit()

	def lesson_list(self):
		cur = self.db.conn.cursor()
		cur.execute(Jpdb._sql['select']['lesson'], (self.id, ))
		lessons = []
		for row in cur.fetchall():
			lessons.append(Lesson(self.db, self, row[0], row[1]))
		return lessons

	def lesson_get(self, title):
		cur = self.db.conn.cursor()
		cur.execute(Jpdb._sql['select']['lesson'] + " AND title = ? ", (self.id, title))
		lesson = cur.fetchone()
		if lesson == None:
			raise LookupError("Could not find the specified lesson.", title)
		return Lesson(self.db, self, lesson[0], lesson[1])


class Lesson:

	# Things that can be associated with lessons:
	# word powers (with and without pictures), singular words, dialogues, homeworks, 

	def __init__(self, db, book, id, title):
		self.db = db
		self.book = book
		self.id = id
		self.title = title

	def header():
		return (
			"  id  title\n" +
			"-------------"
		)
	def __str__(self):
		return "{:>4}  {}".format(self.id, self.title)
	def doprint(lessons):
		print(Lesson.header())
		if type(lessons) != list:
			lessons = [ lessons ]
		for lesson in lessons:
			print(lesson)

	def update(self):
		cur = self.db.conn.cursor()
		cur.execute(Jpdb._sql['update']['lesson'], (self.title, self.id))
		self.db.conn.commit()



def parseargs():
	value_db = './db.sqlite'

	parser = argparse.ArgumentParser()
	parser.add_argument('--db', type=str, default=value_db,
	                    help="Path to the database. default: '{}'".format(value_db))
	parser.add_argument('--book', type=str, default=None,
	                    help="Book to operate on.")
	parser.add_argument('--lesson', type=str, default=None,
	                    help="Lesson to operate on.")
	parser.add_argument('action', type=str, choices=['init', 'info',
	                                                 'book-new', 'book-list', 'book-update',
	                                                 'lesson-new', 'lesson-list', 'lesson-update'
	                                                ],
	                    help="What you want the script to do.")
	parser.add_argument('value', type=str, nargs='?',
	                    help="Some value to pass to the current action.")

	return parser.parse_args()

if __name__ == "__main__":
	import argparse
	argv = parseargs()
	book = None
	lesson = None

	with Jpdb(argv.db) as db:
		if (argv.book != None):
			book = db.book_get(argv.book)
		if (argv.lesson != None):
			lesson = book.lesson_get(argv.lesson)


		if (argv.action.startswith('lesson') and book == None):
			raise ValueError("'lesson' actions require a '--book' to operate on.")


		# generic actions

		if (argv.action == 'init'):
			db.init()
		if (argv.action == 'info'):
			db.info()


		# book actions

		if (argv.action == 'book-new'):
			if (argv.value == None):
				raise ValueError("Action {} requires an additional value.".format(argv.action))
			db.book_create(argv.value)
		if (argv.action == 'book-list'):
			if (book == None):
				directory = db.book_list()
				Book.doprint(directory)
			else:
				Book.doprint(book)
		if (argv.action == 'book-update'):
			if (argv.value == None):
				raise ValueError("Action {} requires an additional value.".format(argv.action))
			if (book == None):
				raise ValueError("Action {} requires a '--book' be specified.".format(argv.action))
			book.title = argv.value
			book.update()


		# lesson actions

		if (argv.action == 'lesson-new'):
			if (argv.value == None):
				raise ValueError("Action {} requires an additional value.".format(argv.action))
			book.lesson_create(argv.value)
		if (argv.action == 'lesson-list'):
			if (lesson == None):
				directory = book.lesson_list()
				Lesson.doprint(directory)
			else:
				Lesson.doprint(lesson)
		if (argv.action == 'lesson-update'):
			if (argv.value == None):
				raise ValueError("Action {} requires an additional value.".format(argv.action))
			if (lesson == None):
				raise ValueError("Action {} requires a '--lesson' be specified.".format(argv.action))
			lesson.title = argv.value
			lesson.update()
